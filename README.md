# Propagación de las Ondas Electromagnéticas

## Sílabo

1. [Introducción](1.%20Introduccion.md)
2. [Ondas Transversales Electromagnéticas](2.%20Ondas%20Transversales%20Electromagn%C3%A9ticas.md)
3. [El espectro radio eléctrico](3.%20El%20espectro%20radio%20el%C3%A9ctrico.md)
4. [Polarización Electromagnética](4.%20Polarizaci%C3%B3n%20Electromagn%C3%A9tica.md)
5. [Rayos y Frentes de Onda](5.%20Rayos%20y%20Frentes%20de%20onda.md)
6. [Radiación Electromagnética](6.%20Radiaci%C3%B3n%20Electromagn%C3%A9tica.md)
7. [Frente de Onda esférico y la ley del cuadrado](7.%20Frente%20de%20onda%20esf%C3%A9rico%20y%20la%20ley%20del%20cuadrado%20inverso.md)
8. Atenuación y Absorción de Ondas
9. Propiedades ópticas de las ondas de radio
10. Propagaciòn terrestre de las ondas electromagnéticas
11. Términos y definiciones de propagación
12. Pérdidas en Trayectoria por el espacio libre
13. Margen de desvanecimiento
14. Presupuesto del enlace
15. Diversidad

Referencias:

- Wayne Tomasi (2001), Sistemas de Comunicaciones Electrónicas, Cuarta Edición, Prentice Hall.
- Wayne Tomasi (2014), Advanced Electronic Communications
Systems, Sexta Edición, Person Education.